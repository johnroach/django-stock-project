
# Renko Stock Panel

This program is a simple stock program for warehouses and such.

It uses Django and it has the following dependencies ;

    - python-django
    - python-mysqldb
    - python-docutil
    - django-crispy-forms : https://github.com/maraujop/django-crispy-forms
    - unipath : http://pypi.python.org/pypi/Unipath
    
    
The collation of the database must be :

    Collation 	: utf8_turkish_ci

# Model view 

![](https://bitbucket.org/johnroach/django-stock-project/raw/e356be09da8967acd7b7a9dc377db43daf025b41/myapp_models.png)
