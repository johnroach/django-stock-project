# -*- coding: utf-8 -*-

from django.db import models
from django.utils import encoding

# Create your models here.sasd
class Category(models.Model):
    name = models.CharField(max_length=250)
    exp = models.TextField()
    ancestor = models.IntegerField(max_length=11)
    ancestor.null = True
    ancestor.blank = True
    class Meta:
        verbose_name = u"kategori"
        verbose_name_plural = u"kategoriler"
    def __unicode__(self):
        return encoding.force_unicode(self.name, encoding='utf-8', strings_only=True, errors='strict')