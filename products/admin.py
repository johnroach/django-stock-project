from products.models import Product,ProductType
from products.models import Linked
from django.contrib import admin

admin.site.register(Product)
admin.site.register(Linked)
admin.site.register(ProductType)
