# -*- coding: utf-8 -*-

from django.db import models
from categories.models import Category
from django.utils import encoding

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=250)
    bar_code = models.CharField(max_length=250)
    model = models.CharField(max_length=200)
    type = models.CharField(max_length=25)
    class Meta:
        verbose_name = u"ürün"
        verbose_name_plural = u"ürünler"
    def __unicode__(self):
        return encoding.force_unicode(self.name, encoding='utf-8', strings_only=True, errors='strict')

class ProductType(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = u"ürün1"
        verbose_name_plural = u"ürünler2"
    def __unicode__(self):
        return self.name
    
class Linked(models.Model):
    product_id = models.ForeignKey(Product)
    category_id = models.ForeignKey(Category)
    class Meta:
        verbose_name = u"ürün kategori bağlantısı"
        verbose_name_plural = u"ürün kategori bağlantıları"
    def __unicode__(self):
        return self.product_id
