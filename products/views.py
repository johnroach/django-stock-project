# Create your views here.
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from forms import ProductForm
from models import Product
from django.contrib.auth.decorators import permission_required

@login_required
def list_stocks(request):
    #calculation
    total_cost = 5 * 20
    #import pdb;pdb.set_trace() -- sets a debugging point
    products = Product.objects.all()
    return render_to_response('list_stocks.html',{'products':products},context_instance=RequestContext(request))


@login_required
@permission_required('products.add_product')
def add_product(request):

    form = ProductForm()
    if request.POST:
        form = ProductForm(request.POST)
        if form.is_valid():
            product = Product()
            product.name = form.cleaned_data['name']
            product.bar_code = form.cleaned_data['bar_code']
            product.model = form.cleaned_data['model']
            product.type = form.cleaned_data['product_type']
            product.save()
            products = Product.objects.all()
            return render_to_response('list_stocks.html',{'products':products},context_instance=RequestContext(request))
            
    return render_to_response('add_product.html',{'product_form':form},context_instance = RequestContext(request))

@login_required
def delete_product(request):

    id = request.GET['id']
    Product.objects.get(id=id).delete()
    products = Product.objects.all()
    return render_to_response('list_stocks.html',{'products':products},context_instance=RequestContext(request))


@login_required
def edit_product(request):

    id = request.GET['id']
    product = Product.objects.get(id=id)
    if request.POST:
        form = ProductForm(request.POST)
        if form.is_valid():
            product.name = form.cleaned_data['name']
            product.bar_code = form.cleaned_data['bar_code']
            product.model = form.cleaned_data['model']
            product.type = form.cleaned_data['product_type']
            product.save()
            products = Product.objects.all()
            return render_to_response('list_stocks.html',{'products':products},context_instance=RequestContext(request))
    form = ProductForm(initial = {'name':product.name,'bar_code':product.bar_code,'model':product.model,'type':product.type})
    return render_to_response('edit_product.html',{'product_form':form},context_instance = RequestContext(request))





