from django import forms
from models import ProductType
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class ProductForm(forms.Form):
    name = forms.CharField(required=True)
    bar_code = forms.CharField()
    model = forms.CharField()
    product_type = forms.ModelChoiceField(queryset=ProductType.objects.all(), required=True,initial=0)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_id = 'id-addproduct'
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''

        self.helper.add_input(Submit('submit', 'Submit'))
        super(ProductForm, self).__init__(*args, **kwargs)
    
    
