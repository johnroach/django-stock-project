# -*- coding: utf-8 -*-

from django.db import models
from django.utils import encoding

# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=250)
    tel_1 = models.CharField(max_length=20)
    tel_2 = models.CharField(max_length=20)
    fax = models.CharField(max_length=20)
    address = models.TextField()
    contact_person_name_lastname = models.CharField(max_length=250)
    contact_person_exp = models.TextField()
    tax_number = models.CharField(max_length=40)
    tax_building = models.CharField(max_length=250)
    class Meta:
        verbose_name = u"şirket"
        verbose_name_plural = u"şirketler"
    def __unicode__(self):
        return encoding.force_unicode(self.name, encoding='utf-8', strings_only=True, errors='strict')