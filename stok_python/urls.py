from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'stok_python.views.home', name='home'),
    # url(r'^stok_python/', include('stok_python.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','products.views.list_stocks'),
    url(r'^addproduct/$','products.views.add_product'),
    url(r'^deleteproduct/$','products.views.delete_product'),
    url(r'^editproduct/$','products.views.edit_product'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
)
