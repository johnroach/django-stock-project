# -*- coding: utf-8 -*-

from django.db import models
from companies.models import Company
from products.models import Product
from django.utils import encoding

class PaymentType(models.Model):
    name = models.CharField(u"ödeme şekli adı", max_length=50)    
    class Meta:
        verbose_name = u"ödeme şekli"
        verbose_name_plural = u"ödeme şekilleri"
    def __unicode__(self):
        return  self.producttype

# Create your models here.
class Bill(models.Model):
    BILL_TYPE = (
    (u'in', u'alım'),
    (u'ou', u'satış'),
    )
    company = models.ForeignKey(Company)
    number = models.CharField(u"fatura no",max_length=250)
    payment_type = models.ForeignKey(PaymentType,verbose_name=u"ödeme şekli")
    type_in_out = models.CharField(u"fatura tipi", max_length=2,choices=BILL_TYPE)
    date_of_bill = models.DateField(u"fatura tarihi")
    class Meta:
        verbose_name = u"fatura"
        verbose_name_plural = u"faturalar"
    def __unicode__(self):
        return  encoding.force_unicode(self.number, encoding='utf-8', strings_only=True, errors='strict')

class Productsin(models.Model):
    category_id = models.ForeignKey(Bill)
    product_id = models.ForeignKey(Product)
    class Meta:
        verbose_name = u"ürün fatura bağlantısı"
        verbose_name_plural = u"ürün fatura bağlantıları"
    def __unicode__(self):
        return self.productsin   

