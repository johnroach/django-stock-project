from bills.models import Bill
from bills.models import PaymentType
from bills.models import Productsin

from django.contrib import admin

admin.site.register(Bill)
admin.site.register(PaymentType)
admin.site.register(Productsin)